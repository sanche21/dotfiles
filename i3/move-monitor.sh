#!/bin/bash

ACTIVE_WORKSPACE=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name' | cut -d"\"" -f2)
BASE_WORKSPACE=$(echo $ACTIVE_WORKSPACE | rev | cut -d":" -f1 | rev)
echo $BASE_WORKSPACE

SELECTION=$(zenity --list --column Monitor "Unlocked" "1: Primary" "2: Secondary" "0: Satelite")

if [[ ! -z $SELECTION ]]; then
    NUMBER=$(echo $SELECTION | grep -o "[0-9]:")

    echo $NUMBER
    if [[ ! -z "$NUMBER" ]]; then
        NUMBER=$NUMBER
    fi

    i3-msg rename workspace to $NUMBER$BASE_WORKSPACE
    ~/.i3/multimonitor-refresh-state.sh
fi
