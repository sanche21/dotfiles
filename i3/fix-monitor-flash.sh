#!/bin/bash

# lock ultrawide monitor to 60fps

MONITOR_NAME=$(xrandr | grep 3440x1440 | cut -d' ' -f 1)
xrandr --output $MONITOR_NAME  --mode 3440x1440 --rate 60
