#!/bin/bash

ACTIVE_WORKSPACE=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name' | cut -d"\"" -f2)
BASE_WORKSPACE=$(echo $ACTIVE_WORKSPACE | rev | cut -d":" -f1 | rev)
WORKSPACE_PREFIX=$(echo $ACTIVE_WORKSPACE | grep -o "[0-9]:")

echo "base: $BASE_WORKSPACE prefix: $WORKSPACE_PREFIX"

NEW_NAME=$(zenity --entry --text "Rename workspace to:")
if [[ ! -z "$NEW_NAME" ]]; then
    i3-msg rename workspace to $WORKSPACE_PREFIX$NEW_NAME
fi
