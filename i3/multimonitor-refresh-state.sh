#!/bin/bash
cd $(dirname $0)

# declare labels for all monitors
declare -A displaymap
for f in ./monitor-map/*; do
    m=$(basename $f)
    l=$(cat $f)
    if [[ ! -z $l ]]; then
        displaymap[$m]=$l
        echo "found monitor: $m label: $l"
    fi
done

ACTIVE_MONITOR=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).output' | cut -d"\"" -f2)
CONNECTED_DISPLAYS=$(i3-msg -t get_workspaces | jq '.[] | select(.visible=true).output' | cut -d"\"" -f2 | uniq)
IS_NUMBER='^[0-9]+$'

# find primary monitor
# preference order: any, 0, 2, 1
PRIMARY_DISPLAY=$ACTIVE_MONITOR
for d in $CONNECTED_DISPLAYS ; do
    if [[ ${displaymap[$d]+_} && ${displaymap[$d]} -eq 0 ]]; then PRIMARY_DISPLAY=$d; fi
done
for d in $CONNECTED_DISPLAYS ; do
    if [[ ${displaymap[$d]+_} && ${displaymap[$d]} -eq 2 ]]; then PRIMARY_DISPLAY=$d; fi
done
for d in $CONNECTED_DISPLAYS ; do
    if [[ ${displaymap[$d]+_} && ${displaymap[$d]} -eq 1 ]]; then PRIMARY_DISPLAY=$d; fi
done
echo primary display: $PRIMARY_DISPLAY


for row in $(i3-msg -t get_workspaces | jq -c '.[]'); do
    _jq() {
     echo ${row} | jq -r ${1}
    }

    NAME=$(_jq '.name')
    echo
    echo $NAME

    # assign coded workspaces to proper display
    for i in $(seq 0 5) ; do
        if [[ $(echo $NAME | grep -o "[0-9]:") == "$i:" ]]; then
          # found monitor code. Find matching monitor
          for d in $CONNECTED_DISPLAYS ; do
            if [[ ${displaymap[$d]+_} && ${displaymap[$d]} -eq $i ]]; then
                # display with matching label connected
                found="True"
                echo moving $NAME to display $d
                i3-msg [workspace=$NAME] move workspace to output $d
            fi
        done
       fi
    done
    # move non-number, non-pinned workspaces to primary display
    if [[ -z ${found+x} && ! $NAME =~ $IS_NUMBER ]]; then
        echo "not found $NAME"
        i3-msg [workspace="$NAME"] move workspace to output $PRIMARY_DISPLAY
    fi
    unset found
done

# get rid of any empty workspaces
for d in $CONNECTED_DISPLAYS ; do
    i3-msg focus output $d
    WINDOW_COUNT=$(xdotool search --all --onlyvisible \
                    --desktop $(xprop -notype -root _NET_CURRENT_DESKTOP | \
                    cut -c 24-) "" 2>/dev/null | \
                    wc -l)
    if [[ WINDOW_COUNT -eq 0 ]]; then
        i3-msg workspace prev_on_output
    fi
done
i3-msg focus output $PRIMARY_DISPLAY
