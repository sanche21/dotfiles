#!/bin/bash
COMMAND=$@
if [[ -z $COMMAND ]]; then
    COMMAND=gnome-terminal
fi
echo $COMMAND

START_WINDOWS=$(xdotool search --all --onlyvisible --desktop $(xprop -notype -root _NET_CURRENT_DESKTOP | cut -c 24-) "" 2>/dev/null)
$COMMAND &
sleep 0.5
END_WINDOWS=$(xdotool search --all --onlyvisible --desktop $(xprop -notype -root _NET_CURRENT_DESKTOP | cut -c 24-) "" 2>/dev/null)
NEW_WINDOW=$(echo -e "$START_WINDOWS\n$END_WINDOWS" | sort | uniq -u)

ACTIVE_MONITOR=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).output' | cut -d"\"" -f2)
RESOLUTION=$(xrandr |grep "^DP-1-1" | grep -oh "[0-9]\+x[0-9]\+")
HEIGHT=$(echo $RESOLUTION | cut -d"x" -f2)
WIDTH=$(echo $RESOLUTION | cut -d"x" -f1)

i3-msg [id=$NEW_WINDOW] floating enable
i3-msg [id=$NEW_WINDOW] resize set $(python -c  "print('%i %i' % (int($WIDTH*0.6),int($HEIGHT*0.8)))")
i3-msg [id=$NEW_WINDOW] move position center
# randomly reposition
if [[ $(( $RANDOM % 2 )) -eq 1 ]]; then
    i3-msg [id=$NEW_WINDOW] move left $(( RANDOM % 500 )) px
else
    i3-msg [id=$NEW_WINDOW] move right $(( RANDOM % 500 )) px
fi
if [[ $(( $RANDOM % 2 )) -eq 1 ]]; then
    i3-msg [id=$NEW_WINDOW] move up $(( RANDOM % 100 )) px
else
    i3-msg [id=$NEW_WINDOW] move down $(( RANDOM % 100 )) px
fi
