#!/bin/bash
PROFILE=$1
ICON=~/.config/i3/lock-icon.png
WALLPAPER=~/.config/i3/lock-bg.png
TMPBG=/tmp/screen.png

if [[ $PROFILE == "WORK" ]]; then
    xset dpms force off
    /usr/share/goobuntu-desktop-files/xsecurelock.sh
else
    if [ ! -f $WALLPAPER ]; then
        # if no background image provided, generate a new one
        scrot $TMPBG
        convert $TMPBG -scale 2% -scale 5000% $TMPBG
        WALLPAPER=$TMPBG
    fi
    # add lock icon to bg wallpaper
    convert $WALLPAPER $ICON -gravity center -composite -matte $TMPBG
    # lock screen using background
    i3lock -i $TMPBG -c "#ffffff" && xset dpms force off
fi
