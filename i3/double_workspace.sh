#!/bin/bash

NEW_NAME=$(zenity --entry --text "workspace name:")
if [[ ! -z $NEW_NAME ]]; then
    i3-msg workspace 1:$NEW_NAME
    gnome-terminal &
    sleep 0.2
    i3-msg workspace 2:$NEW_NAME
    google-chrome &
    sleep 0.2
    ~/.i3/multimonitor-refresh-state.sh
    i3-msg workspace 2:$NEW_NAME
    i3-msg workspace 1:$NEW_NAME

fi
