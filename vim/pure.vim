"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" USABILITY ENHANCERS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" autoindent
set ai
filetype plugin indent on "filetype-specific indents

"run tig in tmux with G
nnoremap G :silent !tmux split-window  "cd $(dirname $(realpath %)) ; $(which tig) status" <cr>

"change indents when highlited with tab/shift-tab:
vmap <Tab> >gv
vmap <S-Tab> <gv

"keep undo history
if has('nvim')
    set undodir=~/.vim/undodir_nvim
else
    set undodir=~/.vim/undodir
endif
set undofile
"U=redo
nnoremap U <C-R>

"paste toggle
nnoremap <Leader>P :set invpaste paste?<CR>
set pastetoggle=<Leader>P
set showmode

"tab=4 spaces
set tabstop=4       " the width of an actual tab character
set softtabstop=4   " number of spaces in tab in insert mode
set expandtab       " tabs are spaces
set shiftwidth=4    "  how many columns text is indented with the reindent operations (<< and >>)

" remove chronological history panel
map q: :q

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NAVIGATION
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"enable mouse use
set mouse=a

"set jk to work as esc
inoremap jk <esc>

"set i to move to the right
nnoremap i a

"speed movement
map J }
map K {
map L w
map H e

"allow arrow presses to cross between lines
set whichwrap+=<,>,h,l,[,]

"makes backspace work as expected
set backspace=2

let g:netrw_banner = 0
let g:netrw_liststyle = 3

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SPLITS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"split creatiom
map <leader>\| :Vexplore <cr>
map <leader>\\ :vsplit <cr>
map <leader>- :split <cr>
map <leader>_ :Sexplore <cr>

" split movement
nnoremap <Leader><Down> <C-W><C-J>
nnoremap <Leader>j <C-W><C-J>
nnoremap <Leader><Up> <C-W><C-K>
nnoremap <Leader>k <C-W><C-K>
nnoremap <Leader><Right> <C-W><C-L>
nnoremap <Leader>l <C-W><C-L>
nnoremap <Leader><Left> <C-W><C-H>
nnoremap <Leader>h <C-W><C-H>

" small splits: https://www.destroyallsoftware.com/file-navigation-in-vim.html
set winwidth=84
set winheight=5
set winminheight=5
set winheight=999


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" COMPATIBILITY FIXES
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"use filetype specific files (ftplugin)
filetype plugin on

"required for advanced features, not exactly sure what
set nocompatible


"fixes mouse issues in tmux
"if &term =~ '^screen'
"    set ttymouse=xterm2
"endif
" fix for mouse deadzone on wide screens
" https://stackoverflow.com/questions/7000960/in-vim-why-doesnt-my-mouse-work-past-the-220th-column
if !has("nvim")
    set ttymouse=sgr
    if has("mouse_sgr")
        set ttymouse=sgr
    else
        set ttymouse=xterm2
    end
endif

"use system pasteboard mac
if has('mac')
    set clipboard=unnamed
else
    set clipboard=unnamedplus
endif

"bind contol c and control v to copy and paste on mac
if has('mac') == 0
    inoremap <C-v> <C-r>+
    vnoremap <C-c> y
    nnoremap <C-c> yy
    nnoremap <C-v> p
    vnoremap <C-v> p
    vnoremap <C-x> d
    nnoremap <C-x> dd
endif

"fix delay issue
set timeoutlen=1000 ttimeoutlen=10


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" VISUAL IMPROVEMENTS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"solarized dark theme
set background=dark
colorscheme solarized

"highlight current line
set cursorline

"enable line numbers
set number

"enable syntax processing
syntax enable

"highlight matching bracket pairs
set showmatch

"remove the -- INSERT -- text from the status bar. Powerline makes the mode clear enough
set noshowmode 

" Show tabs and trailing spaces characters
if &encoding == "utf-8"
    set lcs=tab:↦\ ,trail:·,nbsp:_,extends:↹,precedes:↹
else
    set lcs=tab:>-,trail:-,nbsp:_,extends:>,precedes:<
endif
set list
hi SpecialKey ctermbg=NONE guibg=NONE

" don't wrap lines
set nowrap

" show autocomplete menu
set wildmenu


"use vertical diff in diff mode
set diffopt+=vertical
"+ stages hunk in diff
vnoremap + :diffput<cr> 

"search
set incsearch   " search as characters are entered
set hlsearch    " highlight matches

" smart line numbers
" https://github.com/jeffkreeftmeijer/vim-numbertoggle/blob/main/plugin/number_toggle.vim
"augroup numbertoggle
"  autocmd!
"  autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu   | endif
"  autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu                  | set nornu | endif
"augroup END
