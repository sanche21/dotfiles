" vim-markdown plugin
let g:markdown_mapping_switch_status = '<Leader><space>'
let g:markdown_enable_input_abbreviations = 0

"autosave markdown (if 'vim-scripts/vim-auto-save' installed)
let g:auto_save = 1

"markdown settings
nnoremap <buffer> r :exe ":ConqueTermSplit grip --pass 2d3b05a51220b008aa4661dbc6a7de12f2af56bd -b " . getreg("%") . "" <cr>
"unindent using ` (right above tab)
inoremap ` <C-d>
"inoremap ~ ``<left>
"map various math characters for markdown
inoremap <Leader>< ≤
inoremap <Leader>> ≥
inoremap <Leader>!= ≠
inoremap <Leader>+- ±
inoremap <Leader>sq √()<left>
inoremap <Leader>rt √()<left>
inoremap <Leader>e ∈
inoremap <Leader>S Σ<sub>i→∞</sub>
inoremap <Leader>I ∫<sub>a→b</sub>
inoremap <Leader>P ∏<sub>i→∞</sub>
inoremap <Leader>U ∪<sub>i→∞</sub>
inoremap <Leader>UU ∩<sub>i→∞</sub>
inoremap <Leader>0 ∅
inoremap <Leader>u ∪
inoremap <Leader>uu ∩
inoremap <Leader>8 ∞
inoremap <Leader>^0 ⁰
inoremap <Leader>^1 ¹
inoremap <Leader>^2 ²
inoremap <Leader>^3 ³
inoremap <Leader>^4 ⁴
inoremap <Leader>^5 ⁵
inoremap <Leader>^6 ⁶
inoremap <Leader>^7 ⁷
inoremap <Leader>^8 ⁸
inoremap <Leader>^9 ⁹
inoremap <Leader>^+ ⁺
inoremap <Leader>^- ⁻
inoremap <Leader>^i ⁱ
inoremap <Leader>^j ʲ
inoremap <Leader>^n ⁿ
inoremap <Leader>^t ᵗ
inoremap <Leader>^x ˣ
inoremap <Leader>^y ʸ
inoremap <Leader>v0 ₀
inoremap <Leader>v1 ₁
inoremap <Leader>v2 ₂
inoremap <Leader>v3 ₃
inoremap <Leader>v4 ₄
inoremap <Leader>v5 ₅
inoremap <Leader>v6 ₆
inoremap <Leader>v7 ₇
inoremap <Leader>v8 ₈
inoremap <Leader>v9 ₉
inoremap <Leader>v+ ₊
inoremap <Leader>v- ₋
inoremap <Leader>vx ₓ
inoremap <Leader>vy ᵧ
inoremap <Leader>vi ᵢ
inoremap <Leader>vj ⱼ
inoremap <Leader>vn ₙ
inoremap <Leader>v <sub></sub><left><left><left><left><left><left>
inoremap <Leader>^ <sup></sup><left><left><left><left><left><left>
inoremap <Leader>-> →
inoremap <Leader><- ←
inoremap <Leader>A ∀
inoremap <Leader>E ∃
inoremap <Leader>... ∴
inoremap <Leader>. ∘
inoremap <Leader>( ⊂
inoremap <Leader>) ⊃
inoremap <Leader>(= ⊆
inoremap <Leader>)= ⊇
inoremap <Leader>f ∫
inoremap <Leader>D ∆
inoremap <Leader>` ∼
inoremap <Leader>O Ω
inoremap <Leader>a α
inoremap <Leader>b β
inoremap <Leader>T θ
inoremap <Leader>t τ
inoremap <Leader>l λ
inoremap <Leader>u μ
inoremap <Leader>m μ
inoremap <Leader>p π
inoremap <Leader>pp Φ
inoremap <Leader>aa σ
inoremap <Leader>std σ
inoremap <Leader>d ∂
inoremap <Leader>Z ℤ
inoremap <Leader>R ℝ
inoremap <Leader>1/2 ½
inoremap <Leader>1/4 ¼
inoremap <Leader>3/4 ¾
