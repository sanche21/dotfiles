"run programs with r
nnoremap <buffer> r :w <bar> exec "w !chmod u+x %" <bar> exec "w !./%"<cr>

" tab = 2 spaces
setlocal shiftwidth=2
setlocal tabstop=2
setlocal softtabstop=2

" highlight >80 chars
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%120v.\+/
