"run programs with r, debug with D
nnoremap <buffer> r :w <bar> exec "w !make -C %:p:h" <bar> exec "w !./%:r"  <cr>
nnoremap <buffer> d :w <bar> exec "w !make -C %:p:h" <bar>  ConqueGdb %< <cr>

" highlight >80 chars
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%81v.\+/
