" run = r, debug = D, profile = P, test = t
nnoremap <buffer> r :w <bar> exec 'w !bundle exec ruby' shellescape(@%, 1)<cr>
nnoremap <buffer> t :w <bar> exec 'w !bundle exec rspec' shellescape(@%, 1)<cr>

" highlight >80 chars
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%80v.\+/

" tab = 2 spaces
setlocal shiftwidth=2
setlocal tabstop=2
setlocal softtabstop=2
