" run = r, debug = D, profile = P
nnoremap <buffer> r :w <bar> exec 'w !mvn clean verify'<cr>

" highlight >100 chars
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%81v.\+/

setlocal shiftwidth=4
setlocal tabstop=4
set softtabstop=4   " number of spaces in tab in insert mode
