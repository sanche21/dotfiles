" run = r, debug = D, profile = P
nnoremap <buffer> r :w <bar> exec 'w !python' shellescape(@%, 1)<cr>
nnoremap <buffer> D :w <bar> silent !tmux split-window -h  "python -m pudb %" <cr>
"nnoremap <buffer> p :w <bar> exec 'w !python -m cProfile -s time ' shellescape(@%, 1)<cr>

" highlight >80 chars
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%80v.\+/

"syntastic
"python requires pep8-naming and flake8 packages
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_error_symbol = '✗'
let g:syntastic_warning_symbol = '!'

"jedi plugin
let g:jedi#documentation_command = "H"
let g:jedi#completions_enabled = 0
