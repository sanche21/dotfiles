"run programs with r
nnoremap <buffer> r :w <bar> exec 'w !go run' shellescape(@%, 1)<cr>

" use tabs (!)
setlocal noexpandtab
set lcs=tab:\ \ ,trail:·,nbsp:_,extends:↹,precedes:↹,space:·, " option 1: show all space
"set lcs=tab:·\ ,trail:·,nbsp:_,extends:↹,precedes:↹, " option 2: unintrusive tabs
set list

" run gofmt on save
set autoread
au BufWritePost *.go !gofmt -w -s %
