" import our base settings (no plugins)
source ~/.vim/pure.vim

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Plugin 'scrooloose/nerdtree'
Plugin 'ervandew/supertab'
Plugin 'majutsushi/tagbar'
Plugin 'vim-syntastic/syntastic'
Plugin 'airblade/vim-gitgutter'
Plugin 'scrooloose/nerdcommenter'
Plugin 'davidhalter/jedi-vim'
Plugin 'easymotion/vim-easymotion'
Plugin 'jiangmiao/auto-pairs'
Plugin 'chrisbra/csv.vim'
call vundle#end() 

" Powerline
set laststatus=2
set t_Co=256
let g:Powerline_symbols = 'fancy'
set encoding=utf-8

"nerd commenter - comment selected lines with c
vmap c <Leader>c<space>

"easy motion - use lowercase for uppercase
nmap <Space> <Plug>(easymotion-overwin-f)
let g:EasyMotion_smartcase = 1
let g:EasyMotion_startofline = 0 " keep cursor column when JK motion

"open nerd tree file browser and tagbar with I (for IDE mode)
nnoremap I :NERDTreeToggle <bar> TagbarToggle<CR>

"Syntastic Syntax Highlighting
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_error_symbol = '✗'
let g:syntastic_warning_symbol = '!'

"jedi - close vim preview after completion
let g:jedi#auto_close_doc = 1
let g:jedi#documentation_command = "<Leader>K"

"csv display
let g:csv_autocmd_arrange = 1
