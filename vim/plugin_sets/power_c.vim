" import our base settings (no plugins)
source ~/.vim/pure.vim

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Plugin 'vim-scripts/Conque-GDB'
Plugin 'scrooloose/nerdtree'
Plugin 'ervandew/supertab'
Plugin 'majutsushi/tagbar'
Plugin 'vim-syntastic/syntastic'
Plugin 'airblade/vim-gitgutter'
Plugin 'scrooloose/nerdcommenter'
Plugin 'easymotion/vim-easymotion'
Plugin 'jiangmiao/auto-pairs'
call vundle#end() 


" Power line
set laststatus=2
set t_Co=256
let g:Powerline_symbols = 'fancy'
set encoding=utf-8

"nerd commenter - comment selected lines with c
vmap c <Leader>c<space>


"easy motion - use lowercase for uppercase
nmap <Space> <Plug>(easymotion-overwin-f)
let g:EasyMotion_smartcase = 1
let g:EasyMotion_startofline = 0 " keep cursor column when JK motion


"Conque
let g:ConqueTerm_Color = 2         " 1: strip color after 200 lines, 2: always with color
let g:ConqueTerm_CloseOnEnd = 1    " close conque when program ends running
let g:ConqueTerm_StartMessages = 0 " display warning messages if conqueTerm is configured incorrectly


"open nerd tree file browser and tagbar with I (for IDE mode)
nnoremap I :NERDTreeToggle <bar> TagbarToggle<CR>

"Syntastic Syntax Highlighting
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_python_checkers = ['python']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_wq = 0
" Better syntastic interface symbols
let g:syntastic_error_symbol = '✗'
let g:syntastic_warning_symbol = '!'
