#!/bin/bash
# the first space on each display should be marked as floating, and labeled as "float" or "float1", "float2", etc.

DISPLAYS=$(yabai -m query --displays | jq '.[].index')


for DISPLAY in $DISPLAYS; do
  SPACE_IDX=$(yabai -m query --spaces --display $DISPLAY | jq '.[0].index')
  label="main"
  if [ $DISPLAY -ne 1 ]; then
    label=$label$DISPLAY
  fi
  yabai -m space $SPACE_IDX --label $label
  yabai -m config --space $SPACE_IDX layout float
done
sketchybar --reload
