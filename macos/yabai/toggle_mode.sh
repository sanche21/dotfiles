#!/bin/sh

SPACE_NUM=$(yabai -m query --spaces --space | jq -r '.index')

# disallow mode change on first screen
if [[ $SPACE_NUM == 1 ]]; then
  hs -c "hs.alert.show(\"mode change disabled on space $SPACE_NUM\")"
  exit 0
fi

CURRENT_MODE=$(yabai -m query --spaces --space | jq -r '.type')

ALL_MODES=("stack" "bsp" "float")

CURRENT_IDX=$(echo ${ALL_MODES[@]/$CURRENT_MODE//} | cut -d/ -f1 | wc -w | tr -d ' ')
NEXT_IDX=$((($CURRENT_IDX + 1) % ${#ALL_MODES[@]}))
NEXT_MODE=${ALL_MODES[$NEXT_IDX]}
echo $CURRENT_IDX $NEXT_IDX
echo $NEXT_MODE


hs -c "hs.alert.show(\"$NEXT_MODE\")"

yabai -m space --layout $NEXT_MODE &

if [[ $NEXT_MODE == "stack" ]]; then
  # increase gaps for stackline
   yabai -m space --padding abs:0:0:40:0
else
  yabai -m space --padding abs:0:0:0:0
fi
# reload sketchybar (for mode/type symbol suffix)
sleep 0.5
sketchybar --reload

# TODO: change size/position around in float mode?
