#!/bin/sh

EXISTING_SPACES=$(yabai -m query --spaces | jq '.[].id')
hs -c "hs.spaces.addSpaceToScreen($DISPLAY)"

# move to the new space
NUM_SPACES=$(hs -c "return #hs.spaces.spacesForScreen()")
hs -c "hs.spaces.gotoSpace(hs.spaces.spacesForScreen()[$NUM_SPACES])"
