#!/bin/bash
# rename current space

# if we're in a "main" space, exit
CURRENT_SPACE_LABEL=$(yabai -m query --spaces --space | jq '.label')
echo "current label: $CURRENT_SPACE_LABEL"
if [[ "$CURRENT_SPACE_LABEL" == "main"* ]]; then
  hs -c "hs.alert.show('cannot rename main space')"
  echo "cannot rename main space"
  exit
fi

NEW_NAME=`osascript -e "display dialog \"workspace name\" default answer \"\"" | grep -o 'text returned:.*' | cut -d ":" -f 2`
if [ -z "$NEW_NAME" ]; then
  echo "no name given"
  hs -c "hs.alert.show('no name given')"
  exit
fi
echo "new label: $NEW_NAME"
yabai -m space --label $NEW_NAME
sketchybar --reload
