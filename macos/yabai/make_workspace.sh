#!/bin/sh

# TODO: move to zshrc_local?

INPUT_NAME=$(osascript -e "display dialog \"workspace name\" default answer \"\"" | grep -o 'text returned:.*' | cut -d ":" -f 2)
if [ -z "$INPUT_NAME" ]; then
  hs -c "hs.alert.show('no name provided')"
  echo "no name provided"
  exit 1
fi

DISPLAYS=$(yabai -m query --displays | jq '.[].id')
NUM_DISPLAYS=$(echo $DISPLAYS | wc -w)

EXISTING_SPACES=$(yabai -m query --spaces | jq '.[].id')

for DISPLAY in $DISPLAYS; do
  # ignore laptop display if there are 3+ displays
  if [ $NUM_DISPLAYS -ge 3 ] && [ $DISPLAY -eq 1 ]; then
    continue
  fi
  hs -c "hs.spaces.addSpaceToScreen($DISPLAY)"
  # move to the new space
  NUM_SPACES=$(hs -c "return #hs.spaces.spacesForScreen($DISPLAY)")
  hs -c "hs.spaces.gotoSpace(hs.spaces.spacesForScreen($DISPLAY)[$NUM_SPACES])"

  sleep 0.5
  # find the id of the new space
  UPDATED_SPACES=$(yabai -m query --spaces | jq '.[].id')
  NEW_SPACE=$(echo "$UPDATED_SPACES\n$EXISTING_SPACES" | sort -n | uniq -c | grep "1 .*" | sed -e "s/^   1 //")
  EXISTING_SPACES=$UPDATED_SPACES
  # get space idx from id
  NEW_SPACE=$(yabai -m query --spaces | jq ".[] | select(.id == $NEW_SPACE) | .index")
  echo "new space: $NEW_SPACE"
  DISPLAY_INDEX=$(yabai -m query --displays | jq ".[] | select(.id == $DISPLAY) | .index")
  SPACE_NAME=$INPUT_NAME
  if [ $DISPLAY_INDEX -ne 1 ]; then
    SPACE_NAME="$SPACE_NAME-$DISPLAY_INDEX"
  fi
  yabai -m space $NEW_SPACE --label $SPACE_NAME
done
# open terminal on main screen
#open -a iTerm .
sketchybar --reload
