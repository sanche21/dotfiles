#!/bin/bash

NUM_CHANGES=0

# delete any empty spaces
DISPLAYS=$(yabai -m query --displays | jq '.[].index')
for DISPLAY in $DISPLAYS; do
  SPACES=$(yabai -m query --spaces --display $DISPLAY | jq '.[].id')
  counter=0
  for SPACE_ID in $SPACES; do
    SPACE_IDX=$(yabai -m query --spaces --display $DISPLAY | jq ".[] | select(.id == $SPACE_ID) | .index")
    SPACE_NAME=$(yabai -m query --spaces --display $DISPLAY | jq ".[] | select(.id == $SPACE_ID) | .label")
    counter=$((counter+1))
    # ignore first space of each display
    if [ $counter -eq 1 ]; then
      continue
    fi
    echo "checking space: $SPACE_NAME ($SPACE_IDX)"
    WINDOWS=$(yabai -m query --windows --space $SPACE_IDX | jq '.[].id')
    if [ -z "$WINDOWS" ]; then
      echo "  - attempting delete"
      hs -c "hs.spaces.removeSpace($SPACE_ID)"
      NUM_CHANGES=$((NUM_CHANGES+1))
    fi
  done
done
# refresh only if changes were made
if [ $NUM_CHANGES -gt 0 ]; then
  sketchybar --reload
fi
