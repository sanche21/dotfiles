#brew install koekeishiya/formulae/skhd
#brew install koekeishiya/formulae/yabai
#pip install yabai-stack-navigator

brew install jq
brew tap homebrew/cask-fonts
brew install --cask font-hack-nerd-font

brew install FelixKratz/formulae/borders

brew install hammerspoon --cask

brew install --cask raycast

brew tap FelixKratz/formulae
brew install sketchybar

brew install --cask nikitabobko/tap/aerospace

# start borders
borders active_color=0xff59a1ff inactive_color=0x402f343f width=7.0 &
