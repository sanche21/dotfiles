#!/bin/bash
STATE="$(echo "$INFO" | jq -r '.state')"

if [ "$STATE" = "playing" ]; then
  #MEDIA="$(echo "$INFO" | jq -r '.app + ": " + .title + " - " + .artist')"
  MEDIA="$(echo "$INFO" | jq -r '.title + " - " + .artist')"
  if [[ $INFO == *"Spotify"* ]]; then
    COLOR=0xff4aa147
  else
    COLOR=0xffffffff
  fi
  sketchybar --set $NAME label="$MEDIA" drawing=on label.color=$COLOR
else
  sketchybar --set $NAME drawing=off
fi
