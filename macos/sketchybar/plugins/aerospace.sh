#!/usr/bin/env bash

# make sure it's executable with:
# chmod +x ~/.config/sketchybar/plugins/aerospace.sh

active_workspace_for_monitor=$(aerospace list-workspaces --monitor $2 --visible)
if [[ "$active_workspace_for_monitor" =~ "$1"  ]]; then
    sketchybar --set $NAME background.drawing=on
else
    sketchybar --set $NAME background.drawing=off
fi
