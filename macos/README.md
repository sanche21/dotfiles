## ~Yabai~ (Replaced with Aerospace)
- window manager, like i3
- installed through brew, configured through yabairc
  - brew install koekeishiya/formulae/skhd
- opening chrome through shortcut had bugs (would switch spaces). I set up an applescript that manipualtes the dock icon instead
- need external script for navigating stacks
  - https://github.com/sendhil/yabai-stack-navigator
- docs: https://github.com/koekeishiya/yabai/blob/master/doc/yabai.asciidoc

## ~skhd~ (Replaced with Aerospace)
- keyboard shortcuts
- for navigating yabai, launching apps, etc
- brew install koekeishiya/formulae/yabai

## ~Ubersicht~ (Replaced by sketchybar)
- adds widgets to screen
- really just want it for simple-bar status bar
- widgets installed in $HOME/Library/Application\ Support/Übersicht/widgets
- seems a bit buggy. Prefer Sketchybar

## Skeychybar
- a better bar replacement than simple-bar
- requires Hack Nerd Font
- brew tap homebrew/cask-fonts; brew install --cask font-hack-nerd-font
- https://felixkratz.github.io/SketchyBar/setup
- controlled with `brew services start/stop sketchybar`

## Borders
- brew install FelixKratz/formulae/borders
- controlled through yabairc or aerospace config

## Hammerspoon
- bridge between OS automation and lua
- mostly needed for stackline
- also good for printing messages
- brew install hammerspoon --cask
- https://github.com/Hammerspoon/hammerspoon
- controlled through menubar

## ~Stackline~
- show UI for yabai stacks

## Aerospace
- i3-like window manager
- yabai alternative
- big difference: has its own space system instead of relying on macOS spaces
  - makes it faster, and a bit better for multi-monitor
