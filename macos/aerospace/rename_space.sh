#!/bin/bash
# rename current space

# if we're in a "main" space, exit
#CURRENT_SPACE_LABEL=$(yabai -m query --spaces --space | jq '.label')
#echo "current label: $CURRENT_SPACE_LABEL"
#if [[ "$CURRENT_SPACE_LABEL" == "main"* ]]; then
#  hs -c "hs.alert.show('cannot rename main space')"
#  echo "cannot rename main space"
#  exit
#fi

NEW_NAME=`osascript -e "display dialog \"workspace name\" default answer \"\"" | grep -o 'text returned:.*' | cut -d ":" -f 2`
if [ -z "$NEW_NAME" ]; then
  echo "no name given"
  hs -c "hs.alert.show('no name given')"
  exit
fi
echo "new label: $NEW_NAME"

# get active monitor and workspace
active_monitor=$(aerospace list-monitors --focused | awk '{print $1}')
active_workspace=$(aerospace list-workspaces --focused)

# get active workspaces at start
for monitor_id in $(aerospace list-monitors | awk '{print $1}'); do
  if [[ $monitor_id == $active_monitor ]]; then
    # skip active monitor
    continue
  fi
  starting_workspaces=$starting_workspaces" "$(aerospace list-workspaces --monitor $monitor_id --visible)
done
echo $starting_workspaces

# copy windows
windows=$(aerospace list-windows --monitor $active_monitor --workspace $active_workspace | awk '{print $1}')
for window in $windows; do
  echo "window: $window"
  aerospace focus --window-id $window
  aerospace move-node-to-workspace $NEW_NAME
done
# move to proper monitor
aerospace workspace $NEW_NAME
while $(aerospace list-monitors --focused | awk '{print $1}' | grep -q -v $active_monitor); do
  aerospace move-workspace-to-monitor next --wrap-around
  aerospace workspace $NEW_NAME
done
# restore old workspaces
for ws in $starting_workspaces; do
  echo "restoring workspace: $ws"
  aerospace workspace $ws
done
aerospace workspace $NEW_NAME


#yabai -m space --label $NEW_NAME
#WORKSPACE_NUM=$(aerospace list-workspaces --focused | sort -n -k1.2 | head -n 1)
#echo "AEROSPACE_LABEL_$WORKSPACE_NUM=$NEW_NAME" >> ~/.aerospace_labels
sketchybar --reload
