#!/bin/bash

DIRECTION=$1

if [ "$DIRECTION" = "left" ]; then
  aerospace move-workspace-to-monitor prev --wrap-around
elif [ "$DIRECTION" = "right" ]; then
  aerospace move-workspace-to-monitor next --wrap-around
else
  echo "invalid direction"
  exit
fi

sketchybar --reload
