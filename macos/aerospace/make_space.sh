#!/bin/sh


NEW_NAME=`osascript -e "display dialog \"workspace name\" default answer \"\"" | grep -o 'text returned:.*' | cut -d ":" -f 2`
if [ -z "$NEW_NAME" ]; then
  echo "no name given"
  hs -c "hs.alert.show('no name given')"
  exit
fi
echo "new label: $NEW_NAME"

# get active monitor and workspace
active_monitor=$(aerospace list-monitors --focused | awk '{print $1}')
active_workspace=$(aerospace list-workspaces --focused)

# get active workspaces at start
for monitor_id in $(aerospace list-monitors | awk '{print $1}'); do
  if [[ $monitor_id == $active_monitor ]]; then
    # skip active monitor
    continue
  fi
  starting_workspaces=$starting_workspaces" "$(aerospace list-workspaces --monitor $monitor_id --visible)
done
echo $starting_workspaces

# move to proper monitor
aerospace workspace $NEW_NAME
while $(aerospace list-monitors --focused | awk '{print $1}' | grep -q -v $active_monitor); do
  aerospace move-workspace-to-monitor next --wrap-around
  aerospace workspace $NEW_NAME
done
# restore old workspaces
for ws in $starting_workspaces; do
  echo "restoring workspace: $ws"
  aerospace workspace $ws
done
aerospace workspace $NEW_NAME

sleep 0.1
sketchybar --reload
