#!/bin/bash

# Kubernetes prompt helper for bash/zsh
# Displays current context and namespace

# Copyright 2017 Jon Mosco
#
#  Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Debug
[[ -n $DEBUG ]] && set -x

# Default values for the prompt
# Override these values in ~/.zshrc or ~/.bashrc
_KUBE_PS1_BINARY_DEFAULT="${_KUBE_PS1_DEFAULT:-true}"
_KUBE_PS1_BINARY="${_KUBE_PS1_BINARY:-"kubectl"}"
_KUBE_PS1_DISABLE_PATH="${HOME}/.kube/kube-ps1/disabled"
_KUBE_PS1_NS_ENABLE="${_KUBE_PS1_NS_ENABLE:-true}"
_KUBE_PS1_UNAME=$(uname)
_KUBE_PS1_LABEL_ENABLE="${_KUBE_PS1_LABEL_ENABLE:-true}"
_KUBE_PS1_LABEL_DEFAULT="${_KUBE_PS1_LABEL_DEFAULT:-"⎈ "}"
_KUBE_PS1_LABEL_USE_IMG="${_KUBE_PS1_LABEL_USE_IMG:-false}"
_KUBE_PS1_LAST_TIME=0
_KUBE_PS1_PREFIX="("
_KUBE_PS1_SEPARATOR="|"
_KUBE_PS1_DIVIDER=":"
_KUBE_PS1_SUFFIX=")"

if [ "${ZSH_VERSION}" ]; then
  _KUBE_PS1_SHELL="zsh"
elif [ "${BASH_VERSION}" ]; then
  _KUBE_PS1_SHELL="bash"
fi

# TODO: Make the colors have the _KUBE_PS1_ prefix
_kube_ps1_shell_settings() {
  case "${_KUBE_PS1_SHELL}" in
    "zsh")
      setopt PROMPT_SUBST
      autoload -U add-zsh-hook
      add-zsh-hook precmd _kube_ps1_load
      zmodload zsh/stat
      reset_color="%f"
      blue="%F{blue}"
      red="%F{red}"
      cyan="%F{cyan}"
      ;;
    "bash")
      reset_color=$(tput sgr0)
      blue=$(tput setaf 4)
      red=$(tput setaf 1)
      cyan=$(tput setaf 6)
        # TODO: only add it if it's not there
        PROMPT_COMMAND="${PROMPT_COMMAND:-:};_kube_ps1_load"
      ;;
  esac
}

_kube_ps1_binary() {
  if [[ "${_KUBE_PS1_BINARY_DEFAULT}" == true ]]; then
    local _KUBE_PS1_BINARY="${_KUBE_PS1_BINARY_DEFAULT}"
  elif [[ "${_KUBE_PS1_BINARY_DEFAULT}" == false ]] && [[ "${_KUBE_PS1_BINARY}" == "oc" ]];then
    local _KUBE_PS1_BINARY="oc"
  fi

  _KUBE_PS1_BINARY="${_KUBE_PS1_BINARY}"
}

kube_ps1_label() {
  [[ "${_KUBE_PS1_LABEL_ENABLE}" == false ]] && return

  if [[ "${_KUBE_PS1_LABEL_USE_IMG}" == true ]]; then
    local _KUBE_PS1_LABEL_DEFAULT="☸️ "
  fi

  _KUBE_PS1_LABEL="${_KUBE_PS1_LABEL_DEFAULT}"
}

_kube_ps1_split() {
  type setopt >/dev/null 2>&1 && setopt SH_WORD_SPLIT
  local IFS=$1
  echo $2
}

_kube_ps1_file_newer_than() {
  local mtime
  local file=$1
  local check_time=$2

  if [[ "${_KUBE_PS1_SHELL}" == "zsh" ]]; then
    mtime=$(stat +mtime "${file}")
  elif [ x"$_KUBE_PS1_UNAME" = x"Linux" ]; then
    mtime=$(stat -c %Y "${file}")
  else
    mtime=$(stat -f %m "$file")
  fi

  [ "${mtime}" -gt "${check_time}" ]
}

_kube_ps1_load() {
  # kubectl will read the environment variable $_KUBECONFIG
  # otherwise set it to ~/.kube/config
  : "${_KUBECONFIG:=$HOME/.kube/config}"

  for conf in $(_kube_ps1_split : "${_KUBECONFIG}"); do
    if _kube_ps1_file_newer_than "${conf}" "${_KUBE_PS1_LAST_TIME}"; then
      # TODO: Test here for these values being set
      _kube_ps1_get_context_ns
      return
    fi
  done
}

# TODO: Break this function apart:
#       one for context and one for namespace
_kube_ps1_get_context_ns() {
  # Set the command time
  _KUBE_PS1_LAST_TIME=$(date +%s)

  _KUBE_PS1_CONTEXT="$(${_KUBE_PS1_BINARY} config current-context)"
  if [[ -z "${_KUBE_PS1_CONTEXT}" ]]; then
    echo "kubectl context is not set"
    return 1
  fi

  if [[ "${_KUBE_PS1_NS_ENABLE}" == true ]]; then
    _KUBE_PS1_NAMESPACE="$(${_KUBE_PS1_BINARY} config view --minify --output 'jsonpath={..namespace}')"
    # Set namespace to default if it is not defined
    _KUBE_PS1_NAMESPACE="${_KUBE_PS1_NAMESPACE:-default}"
  fi
}

# Set shell options
_kube_ps1_shell_settings

# source our symbol
kube_ps1_label

# Build our prompt
kube_ps1() {
  [ -f "${_KUBE_PS1_DISABLE_PATH}" ] && return

  _KUBE_PS1="${reset_color}$_KUBE_PS1_PREFIX"
  if [[ "${_KUBE_PS1_LABEL_ENABLE}" == true ]]; then
    _KUBE_PS1+="${blue}$_KUBE_PS1_LABEL"
    _KUBE_PS1+="${reset_color}$_KUBE_PS1_SEPARATOR"
  fi
  _KUBE_PS1+="${red}$_KUBE_PS1_CONTEXT${reset_color}"
  if [[ "${_KUBE_PS1_NS_ENABLE}" == true ]]; then
    _KUBE_PS1+="$_KUBE_PS1_DIVIDER"
    _KUBE_PS1+="${cyan}$_KUBE_PS1_NAMESPACE${reset_color}"
  fi
  _KUBE_PS1+="$_KUBE_PS1_SUFFIX"

  echo "${_KUBE_PS1}"
}
