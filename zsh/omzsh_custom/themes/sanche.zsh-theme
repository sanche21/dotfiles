ZSH_THEME_GIT_PROMPT_PREFIX="%{$reset_color%}%{$fg[white]%}["
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}●%{$fg[white]%}]%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_CLEAN="]%{$reset_color%} "
ZSH_THEME_SVN_PROMPT_PREFIX=$ZSH_THEME_GIT_PROMPT_PREFIX
ZSH_THEME_SVN_PROMPT_SUFFIX=$ZSH_THEME_GIT_PROMPT_SUFFIX
ZSH_THEME_SVN_PROMPT_DIRTY=$ZSH_THEME_GIT_PROMPT_DIRTY
ZSH_THEME_SVN_PROMPT_CLEAN=$ZSH_THEME_GIT_PROMPT_CLEAN
ZSH_THEME_HG_PROMPT_PREFIX=$ZSH_THEME_GIT_PROMPT_PREFIX
ZSH_THEME_HG_PROMPT_SUFFIX=$ZSH_THEME_GIT_PROMPT_SUFFIX
ZSH_THEME_HG_PROMPT_DIRTY=$ZSH_THEME_GIT_PROMPT_DIRTY
ZSH_THEME_HG_PROMPT_CLEAN=$ZSH_THEME_GIT_PROMPT_CLEAN

vcs_status() {
    if [[ $(whence in_svn) != "" ]] && in_svn; then
        svn_prompt_info
    elif [[ $(whence in_hg) != "" ]] && in_hg; then
        hg_prompt_info
    else
        git_prompt_info
    fi
}

PROMPT='$PROMPT_PREFIX%2~ $(vcs_status)»%b '
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ] ; then
  PROMPT='%{$fg[blue]%}%n@%m:%{$reset_color%}'$PROMPT
fi

# calculate last command runtime
function preexec() {
  timer=$(($(print -P %D{%s%6.})/1000))
}

function precmd() {
  if [ $timer ]; then
    now=$(($(print -P %D{%s%6.})/1000))
    elapsed=$(($now-$timer))
    unit=ms
    if [[ "$elapsed" -gt 3600000 ]]; then
        elapsed=$(echo "scale=3; $elapsed/3600000" | bc -l)
        unit=h
    elif [[ "$elapsed" -gt 60000 ]]; then
        elapsed=$(echo "scale=2; $elapsed/60000" | bc -l)
        unit=m
    elif [[ "$elapsed" -gt 1000 ]]; then
        elapsed=$(echo "scale=1; $elapsed/1000" | bc -l)
        unit=s
    fi

    export RPROMPT="%F{cyan}⧖ ${elapsed}${unit}%{$reset_color%}"
    unset timer
  else
    unset RPROMPT
  fi
}

function postexec() {
}
