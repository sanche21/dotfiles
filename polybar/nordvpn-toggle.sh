#!/bin/sh

STATUS=$(nordvpn status | grep Status | tr -d ' ' | cut -d ':' -f2)

if [ "$STATUS" = "Connected" ]; then
    notify-send Test "disconnecting nordvpn"
    nordvpn disconnect
else
    notify-send Test "connecting nordvpn"
    nordvpn connect
fi
