#!/bin/bash
set -x
(
  flock 200

  killall -q polybar

  while pgrep -u $UID -x polybar > /dev/null; do sleep 0.5; done

  outputs=$(xrandr --query | grep " connected" | cut -d" " -f1)
  tray_output="eDP-1"

  for m in $outputs; do
    if [[ $m == "DP-1" ]]; then
        tray_output=$m
    fi
  done

  export DEFAULT_NETWORK_INTERFACE=$(ip route | grep '^default' | awk '{print $5}' | head -n1)
  echo $DEFAULT_NETWORK_INTERFACE
  PROFILE=$1
  if [[ $PROFILE == "WORK" ]]; then
    TOP_BAR="worktop"
    BOTTOM_BAR="workbottom"
  else
    TOP_BAR="top"
    BOTTOM_BAR="bottom"
  fi

  for m in $outputs; do
    echo $m
    export MONITOR=$m
    export TRAY_POSITION=none
    if [[ $m == $tray_output ]]; then
      TRAY_POSITION=right
    fi

    polybar --reload $TOP_BAR </dev/null >/var/tmp/polybar-$m.log 2>&1 200>&- &
    disown
    polybar --reload $BOTTOM_BAR </dev/null >/var/tmp/polybar-$m.log 2>&1 200>&- &
    disown
  done
) 200>/var/tmp/polybar-launch.lock
