#!/bin/zsh

#get desktop number
n=$(xdotool get_desktop)

#get current workspace label
labels=$(gsettings get org.gnome.desktop.wm.preferences workspace-names)
if [[ "$labels" == "['']" ]]; then
    labels="['-', '-', '-', '-', '-', '-', '-', '-', '-', '-']"
fi
et=$(python -c "v=\"$labels\"; v = v.replace('[','').replace(']','').replace('\'', '').split(\", \"); print(v[$n])")
echo  $labels
echo  $et

#prompt user for new workspace label
new=$(zenity --entry --entry-text="$et" --title="Workspace label" --text="New label")

if [ "$new" = ""  ] ; then exit; fi

echo $new

#replace the workspace label in our local file
new_labels=$(python -c "v=\"$labels\"; v = v.replace('[','').replace(']','').replace('\'', '').split(\", \"); v[$n]='$new'; print(v)" )
echo $new_labels


#update settings
gsettings set org.gnome.desktop.wm.preferences workspace-names $new_labels
