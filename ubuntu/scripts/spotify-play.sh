#!/bin/bash
PlayPauseCmd="dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause "

 if pgrep -x "spotify" > /dev/null
then
    echo "Spotify is running"
    $PlayPauseCmd
else
    echo "Spotify is not running"
    (/snap/bin/spotify &) && sleep 3 && $PlayPauseCmd
fi
exit
