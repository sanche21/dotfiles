#!/bin/bash

#add all the dotfiles to their expected places
#this script ids idempotent (written so it can be run any number of times to fix the installation without causing problems)
#all files are managed as symlinks, so everything done should be nin-destructive

#ensure .config exists
mkdir ~/.config 2> /dev/null
mkdir ~/.config/pudb 2> /dev/null
mkdir ~/.config/gpt-cli 2> /dev/null

#move to a dotfiles directory
cd ~/dotfiles 2> /dev/null
cd ~/Dotfiles 2> /dev/null
cd ~/DotFiles 2> /dev/null
cd ~/.Dotfiles 2> /dev/null
cd ~/.DotFiles 2> /dev/null
cd ~/.dotfiles 2> /dev/null

#remove old settings

rm -rf ~/.vim 2> /dev/null
rm -rf ~/.config/nvim 2> /dev/null
rm ~/.vimrc 2> /dev/null
rm ~/.tmux.conf 2> /dev/null
rm -rf ~/.tmux 2> /dev/null
rm  ~/.zshrc 2> /dev/null
rm  ~/.zlogin 2> /dev/null
rm  ~/.zlogout 2> /dev/null
rm -rf ~/.zsh 2> /dev/null
#rm -rf ~/.config/htop 2> /dev/null
rm -rf ./tmux/plugins 2> /dev/null
rm ~/.tigrc 2> /dev/null
rm ~/.config/pudb/pudb.cfg
rm ~/.config 2> /dev/null
rm ~/tmp 2> /dev/null
rm ~/.gitconfig 2> /dev/null
rm ~/.gitignore_global 2> /dev/null
rm -rf ~/.config/i3 2> /dev/null
rm -rf ~/.config/awesome 2> /dev/null
rm -rf ~/.i3 2> /dev/null
rm -rf ~/.polybar 2> /dev/null
rm -rf ~/.config/polybar 2> /dev/null
rm  ~/.i3blocks.conf 2> /dev/null
rm -rf ~/.config/gpt-cli 2> /dev/null

#link in settings files
ln -s $(pwd)/vim ~/.vim
ln -s $(pwd)/vim ~/.config/nvim
ln -s $(pwd)/vim/vimrc ~/.vimrc
ln -s $(pwd)/tmux ~/.tmux
ln -s $(pwd)/tmux/tmux.conf ~/.tmux.conf
ln -s $(pwd)/zsh ~/.zsh
ln -s $(pwd)/zsh/zshrc ~/.zshrc
ln -s $(pwd)/zsh/zlogin ~/.zlogin
ln -s $(pwd)/zsh/zlogout ~/.zlogout
ln -s $(pwd)/git/gitconfig ~/.gitconfig
ln -s $(pwd)/git/gitignore_global ~/.gitignore_global
#cp -r htop ~/.config/htop #copy instead of link, because I want a default htop setting, not necessarily a synced one
ln -s $(pwd)/git/tigrc ~/.tigrc
ln -s $(pwd)/pudb/pudb.cfg ~/.config/pudb/pudb.cfg
ln -s $(pwd)/i3 ~/.config/i3
ln -s $(pwd)/i3 ~/.i3
ln -s $(pwd)/polybar ~/.config/polybar
ln -s $(pwd)/polybar ~/.polybar
ln -s $(pwd)/awesome ~/.config/awesome
ln -s $(pwd)/awesome ~/.awesome
ln -s $(pwd)/i3/i3blocks.conf ~/.i3blocks.conf
ln -s $(pwd)/gptrc ~/.config/gpt-cli/gpt.yaml
ln -s $(pwd)/macos/yabai ~/.config/yabai
ln -s $(pwd)/macos ~/.config/skhd
ln -s $(pwd)/macos/hammerspoon ~/.hammerspoon
ln -s $(pwd)/macos/sketchybar ~/.config/sketchybar
ln -s $(pwd)/macos/simplebarrc ~/.simplebarrc
ln -s $(pwd)/macos/aerospace/aerospace.toml ~/.aerospace.toml
ln -s /tmp ~/tmp
#create machine specific local files if they don't exist
if [ ! -f ./zsh/zshrc_local ]; then
	echo "#this file can be used to create machine-specific zshrc lines" > ./zsh/zshrc_local
fi
if [ ! -f ./git/gitconfig_local ]; then
	echo "#this file can be used to create machine-specific gitconfig lines" > ./git/gitconfig_local
fi
if [ ! -f ./i3/i3rc_local.sh ]; then
	echo "#this file can be used to create a machine-specific i3 startup script" > ./i3/i3rc_local.sh
fi
if [ ! -f ./vim/local.vim ]; then
	echo "\"this file can be used to create a machine-specific vim config" > ./vim/local.vim
fi
#install oh my zsh
if [ -d "$ZSH" ]; then
	echo "zsh already installed"
else
	./zsh/install_omzsh.sh
	rm ~/.zshrc
	ln -s $(pwd)/zsh/zshrc ~/.zshrc
	source ~/.zshrc
fi

#install powerline fonts
./powerline-fonts/install.sh

#install tmux plugins
git clone https://github.com/tmux-plugins/tmux-yank ./tmux/plugins/tmux-yank
git clone https://github.com/tmux-plugins/vim-tmux-focus-events.git ./tmux/plugins/tmux-focus

#install vim plugins (if vim exists and in terminal environment)
if [ -x "$(command -v vim)" ] &&  [  -t 1  ]; then
	vim -c 'PluginInstall' -c 'qa!'
fi
